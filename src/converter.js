/**
 * Padding outputs 2 characters always
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */

const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}


module.exports = {
    /**
    * Converts RGB to Hex string
    * @param {number} red 0-255
    * @param {number} green 0-255
    * @param {number} blue 0-255
    * @returns {string} hex value 
    */
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); // 0-255 -> 00-ff
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        const hex = pad(redHex) + pad(greenHex) + pad(blueHex);

        return  hex; // Hex string, 6 characters
    },

    hexToRgb: (hex1, hex2, hex3) => {

        // const red = hex1.toString(10);
        // const green = hex2.toString(10);
        // const blue = hex3.toString(10);
        
        const red = parseInt(hex1, 16);
        const green = parseInt(hex2, 16);
        const blue = parseInt(hex3, 16);

        const rgb = `${red}, ${green}, ${blue}`;

        return rgb;
    }
}